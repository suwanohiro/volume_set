﻿using NAudio.Wave;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using NAudio.CoreAudioApi;
using System.Windows.Forms;

namespace volume_set
{
    public partial class Form1 : Form
    {
        int old_volume = -2;
        VolumeController vc;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Left = 0;
            this.Top = 0;
            vc = new VolumeController();
            timer1.Start();
            timer2.Start();
        }

        private void set_volume(object sender, EventArgs e)
        {
            vc.SetVolume(int.Parse(((Button)sender).Text));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            set_volume(sender, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            set_volume(sender, e);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            set_volume(sender, e);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            set_volume(sender, e);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            set_volume(sender, e);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            set_volume(sender, e);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            set_volume(sender, e);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            set_volume(sender, e);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            set_volume(sender, e);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            set_volume(sender, e);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            set_volume(sender, e);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            vc.Mute();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int volume = vc.GetNowVolume();
            if(volume != old_volume)
            {
                if (volume < 0)
                {
                    volume = 0;
                    label1.Text = "ミュート";
                }
                else
                {
                    label1.Text = volume.ToString();
                }
                progressBar1.Value = 101;
                progressBar1.Value = volume;
                Console.WriteLine(volume);
                if (volume == 100) progressBar1.Value = 101;
                old_volume = volume;
            }
        }

        private void ManualSet(int num)
        {
            string str = label1.Text;
            if (str.All(char.IsDigit))
            {
                int volume = int.Parse(str);
                int set = volume + num;
                if (set > 100) set = 100;
                if (set < 0) set = 0;
                vc.SetVolume(set);
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            ManualSet(1);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            ManualSet(-1);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void label1_DoubleClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            this.TopMost = !this.TopMost;
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            var k = e.KeyCode;
            var s = k.ToString();
            if (k == Keys.F1) this.TopMost = !this.TopMost;
            if (s == "M") vc.Mute();
            if (s == "Up") ManualSet(5);
            if (s == "Down") ManualSet(-5);
            if (s == "Left") ManualSet(-1);
            if (s == "Right") ManualSet(1);
            if (s == "Escape") Application.Exit();

            //groupBox1.Text = e.KeyCode.ToString();
            textBox1.Text = "";
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }
    }

    class VolumeController
    {
        public void SetVolume(int value)
        {
            //音量を変更
            MMDevice device;
            MMDeviceEnumerator DevEnum = new MMDeviceEnumerator();
            device = DevEnum.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
            device.AudioEndpointVolume.MasterVolumeLevelScalar = ((float)value / 100.0f);
        }

        public void Mute()
        {
            MMDevice device;
            MMDeviceEnumerator DevEnum = new MMDeviceEnumerator();
            device = DevEnum.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
            device.AudioEndpointVolume.Mute = !(device.AudioEndpointVolume.Mute);
        }

        public int GetNowVolume()
        {
            MMDevice device;
            MMDeviceEnumerator DevEnum = new MMDeviceEnumerator();
            device = DevEnum.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
            var volume = device.AudioEndpointVolume.MasterVolumeLevelScalar;
            volume *= 100;
            var data = Math.Round(volume, MidpointRounding.AwayFromZero);
            if (device.AudioEndpointVolume.Mute) data = -1;
            Console.WriteLine(data.ToString());
            return int.Parse(data.ToString());
        }
    }
}
